package com.ruoyi.blog.controller;

import com.ruoyi.blog.es.pojo.ArticleEntity;
import com.ruoyi.blog.es.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/blog/es")
public class BlogEsController {
    @Autowired
    @Qualifier("blogArticleService")
    private ArticleService articleService;

    @RequestMapping("/save")
    @ResponseBody
    public String save(String id,String title,String content){
        ArticleEntity articleEntity=new ArticleEntity();
        articleEntity.setId(id);
        articleEntity.setTitle(title);
        articleEntity.setContent(content);
        articleService.save(articleEntity);
        return "OK";
    }
    @RequestMapping("/findById")
    @ResponseBody
    public Object findArticleEntityById(String id){
        ArticleEntity articleEntity=articleService.findArticleEntitiesById(id);
        return articleEntity;
    }
    @RequestMapping("/findByKeyword")
    @ResponseBody
    public Object findByKeyword(String keyword){
        List<ArticleEntity> list=articleService.findByTitleLikeOrContentLike(keyword,keyword);
        return list;
    }
    @RequestMapping("/deleteById")
    @ResponseBody
    public String deleteArticleEntityById(String id){
        int i=articleService.deleteArticleEntitiesById(id);
        return i+"";
    }

}
