package com.ruoyi.blog.es.pojo;

import lombok.Data;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Mapping;

@Document(indexName = "article_index", type="article")
@Data
@Mapping(mappingPath = "article_mapping.json")
public class ArticleEntity {
    private String id;
    private String title;
    private String content;
}
