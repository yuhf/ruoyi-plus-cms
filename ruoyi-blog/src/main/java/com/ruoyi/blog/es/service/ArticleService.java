package com.ruoyi.blog.es.service;

import com.ruoyi.blog.es.dao.ArticleRepository;
import com.ruoyi.blog.es.pojo.ArticleEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("blogArticleService")
public class ArticleService {
    @Autowired
    @Qualifier("blogArticleRepository")
    private ArticleRepository articleRepository;


    public ArticleEntity findArticleEntitiesById(String id){
       return articleRepository.findArticleEntitiesById(id);
    }

    public int deleteArticleEntitiesById(String id){
        return articleRepository.deleteArticleEntitiesById(id);
    }

    public List<ArticleEntity> findByTitleLikeOrContentLike(String title, String content) {
        return articleRepository.findByTitleLikeOrContentLike(title, content);
    }

    public ArticleEntity save(ArticleEntity articleEntity){
        return articleRepository.save(articleEntity);
    }
}
