package com.ruoyi.spider.es.dao;

import com.ruoyi.spider.es.pojo.ArticleEntity;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("spiderArticleRepository")
public interface ArticleRepository extends ElasticsearchRepository<ArticleEntity,String> {

}
