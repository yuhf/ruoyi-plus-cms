package com.ruoyi.spider.es.service;

import com.ruoyi.spider.es.dao.ArticleRepository;
import com.ruoyi.spider.es.pojo.ArticleEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("spiderArticleService")
public class ArticleService {
    @Autowired
    @Qualifier("spiderArticleRepository")
    private ArticleRepository articleRepository;


    public void save(ArticleEntity articleEntity){
        articleRepository.save(articleEntity);
    }
}
